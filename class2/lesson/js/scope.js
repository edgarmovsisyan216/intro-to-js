function myFunc() {
  var something1 = 'hey';
}

// console.log(something1); // will not be defined

(function() {
  var something2 = "hey again";

  function myFunc2(something2) {
    console.log(something2);
  }

  myFunc2('nope');

})()

// console.log(something2);

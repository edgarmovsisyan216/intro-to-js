var expect = chai.expect;

(function () {
  var original = 'hello intro-to-javascript class';

  /**
   * Substring
   * 1. Using substring save the word "hello" to the variable named "hello"
   * 2. Using substring save the phrase "intro-to-javascript" to the variable named "hyphenated"
   */

  var hello;
  var hyphenated;

  /**
   * Replace
   * 1. Using replace, change the word "hello" with "hey" and save it to the modified variable
   * 2. Using replace, change spaces to hyphens
   */

  var modified;


  /* TESTS: DO NO TOUCH */
  describe('Strings', function () {

    describe('.substring', function () {
      it('should read hello', function () {
        expect(hello).to.equal('hello');
      });

      it('should get the middle of the string', function () {
        expect(hyphenated).to.equal('intro-to-javascript');
      });
    });

    describe('.replace', function () {
      it('should replace hello with hey', function () {
        expect(modified).to.equal('hey-intro-to-javascript-class');
      });
    });

  });
})();

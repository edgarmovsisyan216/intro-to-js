var expect = chai.expect;

(function () {

  /**
   * Selection
   * 1. Select the first heading "h1" and save it to the variable "h1"
   * 2. Add the css class name "my-first-heading" to the h1
   * 3. Change the text of the h1 to "Intro to Js Lesson 2:"
   */

  var h1;


  /* TESTS: DO NO TOUCH */
  describe('DOM: Document Object Model', function () {

    describe('Selecting', function () {
      it('should have selected the h1', function () {
        expect(h1).to.have.property('nodeName', 'H1');
      });

      it('should add a class called "my-first-heading" to the h1', function () {
        expect(h1.classList).to.have.length(1);
        expect(h1.classList[0]).to.equal('my-first-heading');
      });

      it('should replace the inner text of the H1 to "Into to Js Lesson 2:"', function () {
        expect(h1).to.have.property('innerText', 'Intro to Js Lesson 2:');
      });
    });

  });
})();

'this is a string'
42.3435 // float
42
undefined
null
['string1', 42, undefined, null] // array OR list
false // boolean

// truthy & falsy
'' // falsy
'-afhas' // truthy
-1 // true
0 // false
1 // true
undefined // false
null // false
{} // true
[] // true
[].length // false

var marker = {
    brand: 'expo',
    type: 'marker',
    size: 5,
    color: 'green'
};

marker = 'not an object anymore';

var markers = [
    {
        brand: 'expo',
        type: 'marker',
        size: 5,
        color: 'green'
    },
    {
        brand: 'expo',
        type: 'marker',
        size: 5,
        color: 'red'
    }
];

function sum(num1, num2) {
    return num1 + num2;
}

var array = [1, 2, 3, 4, 5];

function multiply(numbers) {
    var result = 1;

    for(var i = 0; i < numbers.length; i++) {
        result = result * numbers[i];
    }

    console.log(result);
}

var result = sum(5, 2);

// conditionals
function cond(anything) {
    if (anything === 0) {
        console.log('number 0');
    } else if (anything === '0') {
        console.log('string 0');
    } else {
        console.log('we didn\'t understand');
    }
}

function advCond(anything) {
    if (typeof anything === 'number' && anything === 1) {
        console.log('first conditional fired');
    } else {
        console.log('we didn\'t understand');
    }
}

function switchStatement(someString) {
    switch(someString) {
        case 'dog':
            console.log('hit dog case');
        break;

        case 'cat':
        case 'feline':
            console.log('hit cat case');
        break;
        
        default:
            console.log('hit default case');
        break;
    }
}

function add() {
    return 1 + 2;
}

function funcInAFunc(func) {
    debugger;
    
    if (typeof func === 'function') {
        return func() + 43;
    }
}

funcInAFunc(add); // 46

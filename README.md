# Introduction to Javascript

Make sure you have node installed... http://nodejs.org

## Windows Installation
1. Open cmd
2. `cd Desktop`
3. `git clone https://gitlab.com/trevthewebdev/intro-to-js`
4. `cd intro-to-js`
5. `npm install` - wait until it's done installing
6. `npm start`
7. open browser and go to http:localhost:3208

const express = require('express');
const app = express();

app.use(express.static('./'));

app.listen(3208, function() {
  console.log('app listening on port 3208');
});

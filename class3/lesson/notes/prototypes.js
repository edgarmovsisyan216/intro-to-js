'use strict';

(function () {

  // constructor function
  function Car(details) {
    this.make = details.make;
    this.model = details.model;
    this.color = details.color;
  }

  // applied to all the cars
  Car.prototype = {
    drive: function() {
      console.log('Driving the ' + this.make + ' ' + this.model);
    },

    brake: function() {
      console.log('Stopping the ' + this.color + ' car...');
    }
  }

  var car1 = new Car({
    make: 'Scion',
    model: 'xB',
    color: 'white'
  });

  var car2 = new Car({
    make: 'Toyota',
    model: 'Carolla',
    color: 'Dark Grey'
  });

  car1.drive();
  car1.brake();

  car2.drive();
  car2.brake();

})();

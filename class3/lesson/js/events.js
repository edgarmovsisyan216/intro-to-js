'use strict';

(function () {
  var $cartCountButton = document.getElementById('cart-toggle');

  function handleCartButton(event) {
    console.log(event.which);
    switch(event.which) {
      case 1:
      case 32:
      case 13:
        console.log('I was triggered');
      break;
    }
  }

  $cartCountButton.addEventListener('click', handleCartButton);
  $cartCountButton.addEventListener('keypress', handleCartButton);

})();

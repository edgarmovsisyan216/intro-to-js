'use strict';

(function () {
  console.log('First Line');
  
  setTimeout(function() {
      console.log('Second Line');
  }, 2000);

  console.log('Third Line');

  axios
    .get('https://overtureapp.com/api/v1/games')
    .then(function(response) {
      console.log(response);
      console.log('Fifth Line');
    })
    .catch(function(error) {
      console.log(error);
    });

  axios
    .get('https://overtureapp.com/api/v1/games/lords-of-waterdeep')
    .then(function(response) {
      console.log(response);
    })
    .catch(function(error) {
      console.log(error);
    });

  console.log('Fourth Line');

})();

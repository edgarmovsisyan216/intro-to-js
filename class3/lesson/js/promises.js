'use strict';

(function () {
  var promise = new Promise(function(resolve, reject) {
    var num = randomNum(1, 10);

    if (num > 2) {
      return resolve('some value');
    }

    return reject(new Error('Something went wrong'));
  });

  promise
    .then(function(value) { // the string "some value" from line 8
      console.log(value);
      console.log('\n ----');
    })
    .catch(function(error) { // the new Error from line 11
      console.log(error)
      console.log('\n ----');
    });

  var cardDeclined = false;
  placeOrder()
    .then(cookPatty)
    .then(assembleBurger)
    .then(function(order) {
      console.log('Ding! Burger Done!', order);
    })
    .catch(function(error) {
      console.log(error);
    });

  
  if (!cardDeclined) {
    console.log('Getting napkins');
  
    setTimeout(function() {
      console.log('Drink cup filled with favorite soda');
    }, 2100);
  }


  /* Functions for making a cheese burger */
  function placeOrder() {
    return new Promise(function(resolve, reject) {
      var num = randomNum(1, 10);
      var orderNumber = randomNum(120, 300);

      if (num < 3) {
        cardDeclined = true;
        return reject(new Error('Card Declined'));
      }

      console.log('Order placed! Starting burger...');
      return resolve(orderNumber);
    });
  }

  function cookPatty(orderNumber) {
    return new Promise(function(resolve) {
      var order = {
        orderNumber: orderNumber,
        burger: {
          patty: 'beef'
        }
      };

      setTimeout(function() {
        console.log('Patty has been cooked!');
        return resolve(order);
      }, 2000);
    });
  }

  function assembleBurger(order) {
    return new Promise(function(resolve) {
      order.burger.cheese = 'cheddar';
      order.burger.condiments = ['ketchup', 'mayonnaise', 'mustard'];

      setTimeout(function() {
        console.log('Burger assembled');
        return resolve(order);
      }, 1000);
    });
  }

})();
